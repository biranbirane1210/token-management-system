import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { TokenComponent } from './token/token.component';
import { TokenItemComponent } from './token/token-item/token-item.component';
import { TokenDetailsComponent } from './token/token-item/token-details/token-details.component';
import {MatDialogModule} from "@angular/material/dialog";
import {ReactiveFormsModule} from "@angular/forms";
import { TokenShareComponent } from './token/token-item/token-share/token-share.component';


@NgModule({
  declarations: [
    HomeComponent,
    TokenComponent,
    TokenItemComponent,
    TokenDetailsComponent,
    TokenShareComponent
  ],
  exports: [
    TokenComponent,
    HomeComponent
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    SharedModule,
    MatDialogModule,
    ReactiveFormsModule
  ]
})
export class HomeModule { }
