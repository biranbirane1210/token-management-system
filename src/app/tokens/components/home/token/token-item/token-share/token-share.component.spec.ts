import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TokenShareComponent } from './token-share.component';

describe('TokenShareComponent', () => {
  let component: TokenShareComponent;
  let fixture: ComponentFixture<TokenShareComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TokenShareComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TokenShareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
