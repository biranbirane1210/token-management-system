import { Component } from '@angular/core';
import {FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-token-details',
  templateUrl: './token-details.component.html',
  styleUrls: ['./token-details.component.css']
})
export class TokenDetailsComponent {
  emailFormControl = new FormControl('', [Validators.required, Validators.email]);
}
